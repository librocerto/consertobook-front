import {createRouter, createWebHashHistory} from "vue-router";
import Home from "@/pages/home.vue";
import HelloWorld from "@/components/HelloWorld.vue";

const routes = [
    { path: '/', component: Home },
    {path: '/hello', component: HelloWorld}
]

export const router = createRouter({
    history: createWebHashHistory(),
    routes,
})
// (｡◕‿‿◕｡)
