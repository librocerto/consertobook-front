import {gql, useQuery} from "@urql/vue";
import {computed, toValue} from "vue";

enum Category {
  ADVENTURE = "ADVENTURE",
  STORIES = "STORIES",
  CLASSICS = "CLASSICS",
  CRIME = "CRIME",
  TALES = "TALES",
  FANTASY = "FANTASY",
  HISTORICAL = "HISTORICAL",
  FICTION = "FICTION",
  HORROR = "HORROR",
  HUMOUR = "HUMOUR",
  SATIRE = "SATIRE",
  MYSTERY = "MYSTERY",
  POETRY = "POETRY",
  PLAYS = "PLAYS",
  ROMANCE = "ROMANCE",
  SHORT_STORIES = "SHORT_STORIES",
  THRILLERS = "THRILLERS",
  WAR = "WAR",
  SCIENCE_FICTION = "SCIENCE_FICTION",
  YOUNG = "YOUNG",
  AUTOBIOGRAPHY = "AUTOBIOGRAPHY",
  AND = "AND",
  MEMOIR = "MEMOIR",
  BIOGRAPHY = "BIOGRAPHY",
  ESSAYS = "ESSAYS",
  NOVEL = "NOVEL",
  COMPUTING = "COMPUTING",
  FAIRY = "FAIRY",
  LITERARY_FICTION = "LITERARY_FICTION"
}

export interface Book {
  pk: string
  title: string
  description: string
  tags: Category[]
  owner: string
  isBorrowed: boolean
  language: string
}

export interface User {
  pk: string
  name: string
  lastname: string
  email: string
  password: string
}

export function useBookApi() {
  function getBooks(querystring: string | ref<string>) {
    const query = useQuery<Book[]>({
      query: gql`{
      books(title__contain: $querystring){
        pk
        title
        description
        tags
      }
    }`,
      variable: {
        querystring: toValue<string>(querystring)
      }
    })

    const books = computed<Book[]>(() => {
      return query.data.value?.books
    })

    return {
      ...query,
      value: books.value
    }
  }

  return {
    getBooks
  }
}